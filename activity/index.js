// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
		let num = 3;
		let getCube = num **3;
		console.log("%citem 1 ","color:orange");
		console.log(`The cube of 3 = ${getCube}`);


	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:
		console.log('');
		console.log("%citem 2 ","color:orange");
		const address = ["Guindapunan","Palo","Leyte"];
		const [brgy, municipality, province] = address;
		console.log(`I live in ${brgy} ${municipality}, ${province}`);


	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
		console.log('');
		console.log("%citem 3 ","color:orange");
		const animal = {
			type: "lion",
			eatWhat : "meat",
			weight : "170–230 kg"
		}
		const {type, eatWhat, weight } = animal;
		console.log(`${type}'s are the king of the jungle. It eats ${eatWhat} and weighs up to ${weight}.`);

	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:
		console.log('');
		console.log("%citem 4 ","color:orange");
		const numbers = [1, 2, 3, 4, 5];
		numbers.forEach((number) => console.log(number));


/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:
		console.log('');
		console.log("%citem 5 ","color:orange");

		class Dog {
		constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
			}
		};

		const myNewCar = new Dog("Eugene",4,"Husky");
		console.log(myNewCar);